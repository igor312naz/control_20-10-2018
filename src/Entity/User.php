<?php


// src/Entity/User.php

namespace App\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    private $newPass;

    /**
     * Sets the email.
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->setUsername($email);

        return parent::setEmail($email);
    }

    /**
     * Set the canonical email.
     *
     * @param string $emailCanonical
     * @return User
     */
    public function setEmailCanonical($emailCanonical)
    {
        $this->setUsernameCanonical($emailCanonical);

        return parent::setEmailCanonical($emailCanonical);
    }


    /**
     * @var string
     *
     * @ORM\Column(type="string" , length=1024)
     */
    private $fullName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=1024)
     */
    private $clientType;

    /**
     *
     * @var string
     *
     * @ORM\Column(type="string",length=1024 , nullable=true)
     */
    private $organization_name;


    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @param string $fullName
     * @return User
     */
    public function setFullName(string $fullName)
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->fullName;
    }

    /**
     * @param string $clientType
     * @return User
     */
    public function setClientType(string $clientType)
    {
        $this->clientType = $clientType;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientType()
    {
        return $this->clientType;
    }

    /**
     * @param string $organization_name
     * @return User
     */
    public function setOrganizationName(string $organization_name)
    {
        $this->organization_name = $organization_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrganizationName()
    {
        return $this->organization_name;
    }

    public function getNewPass()
    {
        return $this->newPass;
    }

    /**
     * @param mixed $newPass
     * @return User
     */
    public function setNewPass($newPass)
    {
        $this->newPass = $newPass;
        return $this;
    }


}